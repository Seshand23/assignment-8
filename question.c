#include<stdio.h>

struct Student{
	char fname[10];
	char subject[20];
	float marks;
};

//void storeDetails(int x);
//void printDetails(int x);

int main()
{
	int x;

	printf("Input the number of students : ");
	scanf("%d", &x);

	printf("\n\n");

	if(x < 5)
	{
		printf("There should be a minimum of 5 students\n");
		return 0;
	}

	struct Student st[x];

	for(int i = 0 ; i < x ; i++)
	{
		printf("Enter the first name of student : ");
		scanf("%s", &st[i].fname);

		printf("Enter the subject of student : ");
		scanf("%s", &st[i].subject);

		printf("Enter the mark of student : ");
		scanf("%f", &st[i].marks);
	
		printf("\n");
	}

	printf("\n\n");

	for(int i = 0 ; i < x ; i++)
	{
		printf("Details of student No.%d \n", i+1);
		printf("Student first name : %s \n", st[i].fname);
		printf("Student subject : %s \n", st[i].subject);
		printf("Student marks : %.2f \n", st[i].marks);
	
		printf("\n");
	}
	
	return 0;
}
